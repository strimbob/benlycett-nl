import * as actionTypes from "../controller/actionTypes";
import { combineReducers } from "redux";
import { Pages } from "./PageModel";
import { ViewportModel } from "./ViewportModel";
import { urlToID } from "../controller/utils";

export const PagesReducer = (
  state: Pages = Pages,
  action: Action
): Pages => {
  return state;
};

export const IDReducer = (
  state: PageId = urlToID({ pages: Pages, url: window.location.pathname }),
  action: Action
): PageId => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.ID_UPDATE:
      return payload;
    default:
      return state;
  }
};

export const viewportReducer = (
  state: Viewport = new ViewportModel(null),
  action: Action
): Viewport => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.VIEWPORT_UPDATE:
      return payload;
    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  pages: PagesReducer,
  id: IDReducer,
  viewport: viewportReducer,
});
