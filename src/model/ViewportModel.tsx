import { Record } from "immutable";

export interface ViewportInterface {}
const defaultView: Viewport = {
  width: window.innerWidth,
  height: window.innerHeight,
  deviceType: "unkown",
};
export class ViewportModel
  extends Record(defaultView)
  implements ViewportInterface
{
  constructor(viewport: Viewport | null) {
    const _default = viewport ? viewport : defaultView;
    const deviceType = _default.width >= 600 ? "computer" : "phone";
    super({
      ...viewport,
      deviceType,
    });
  }

  static fromData(viewPort: Viewport) {
    return new ViewportModel({ ...viewPort });
  }
}
