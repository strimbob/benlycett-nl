import React, { useState, Fragment } from "react";
import { ArrowSvg } from "../1-atoms/ArrowSvg";
type Props = {
  viewport: Viewport;
  pages: Pages;
  controllers: ControlerType;
  currentId: PageId;
  clickHandler: (
    controller: (event: React.SyntheticEvent) => void
  ) => (event: React.SyntheticEvent) => void;
};

export const SideBar = (props: Props) => {
  const { pages, clickHandler, controllers, currentId, viewport } = props;

  const [isHidden, setisHidden] = useState(
    viewport.deviceType === "phone" ? true : false
  );

  if (!pages) return null;

  const animation = isHidden ? "animation" : "";
  return (
    <Fragment>
      <div className={`sideBar-wrapper ${animation}`}>
        <div className={"sideBar"}>
          {pages.entrySeq().map((_page: [Id, Page]) => {
            const [id, page] = _page;
            const selectedPage =
              currentId === id ? "sideBar-selected animation" : "";
            if (!page.title) return null;
            return (
              <div
                key={page.key}
                id={id}
                className={`sideBar-title ${selectedPage} `}
                onClick={(event) => {
                  if (viewport.deviceType === "phone") {
                    setisHidden(true);
                  }
                  clickHandler(controllers.updateId)(event);
                }}
              >
                {page.title}
              </div>
            );
          })}
        </div>
        <div className={"sideBar-Bar"}>
          <ArrowSvg
            onClick={() => {
              setisHidden((h) => !h);
            }}
            className={`sideBar-svg ${animation}`}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 48 48"
          />
        </div>
      </div>
      <div className={`overLay ${isHidden ? "" : "animation"}`}></div>
    </Fragment>
  );
};
