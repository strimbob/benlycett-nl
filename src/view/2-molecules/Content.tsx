import React, { useState, useEffect, useLayoutEffect } from "react";

const duration = 700;
type Props = {
  viewport: Viewport;
  currentPage: Page;
  controllers: ControlerType;
  clickHandler: (
    controller: (event: React.SyntheticEvent) => void
  ) => (event: React.SyntheticEvent) => void;
};

export const Content = (props: Props) => {
  const { clickHandler, controllers, currentPage, viewport } = props;
  const [currentData, setCurrentData] = useState(currentPage);
  const [animation, setAnimation] = useState("");
  const nextData = currentPage;

  useEffect(() => {
    if (viewport.deviceType === "phone") {
      setCurrentData(nextData);
    } else {
      if (currentData !== nextData) {
        setAnimation("animation");
        setTimeout(() => {
          setCurrentData(nextData);
          setAnimation("");
        }, duration);
      }
    }
  }, [nextData, currentData]);

  return (
    <main className={"content-wrapper"}>
      <article className={"content"}>
        <section className={"content-section"}>
          <h1 className={`content-title ${animation}`}>
            {currentData.title}{" "}
          </h1>

          <ul className={"content-nav"}>
            <li
              className={"content-nav-back"}
              id={"back"}
              onClick={clickHandler(controllers.navPages)}
            >
              <h1> {"< "}</h1>
            </li>
            <li
              id={"forward"}
              onClick={clickHandler(controllers.navPages)}
            >
              <h1> {"> "}</h1>
            </li>
          </ul>
        </section>
        <section className={"content-section content-section-right"}>
          <h4 className={`content-intro ${animation}`}>
            {currentData.intro}{" "}
          </h4>
          <div className={`content-border top ${animation}`} />
          {/* {          <GalleryImages />} */}
          <div className={`content-gallery`}></div>
          <div className={`content-border bottom ${animation}`} />
        </section>
        <section className={"content-section"}>
          <p className={`content-text ${animation} `}>
            {" "}
            {currentData.paragraph}{" "}
          </p>
        </section>
      </article>
    </main>
  );
};
