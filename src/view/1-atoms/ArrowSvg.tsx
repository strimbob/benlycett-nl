import React from "react";

export interface SVGProps extends React.ComponentPropsWithoutRef<"svg"> {
  specialProp?: string;
}

export const ArrowSvg = (props: SVGProps) => {
  const { specialProp, ...rest } = props;
  return (
    <svg {...rest}>
      <path
        fill="#1f0550"
        d="M10 24L25.5 4 38 4 22.7 24 22.7 24 22.7 24 38 44 25.5 44z"
      />
    </svg>
  );
};
