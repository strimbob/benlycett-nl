import React from "react";

export interface DivProps extends React.ComponentPropsWithoutRef<"div"> {
  specialProp?: string;
}
export function Div(props: DivProps) {
  const { specialProp, ...rest } = props;
  return <div {...rest}>{rest.children}</div>;
}
