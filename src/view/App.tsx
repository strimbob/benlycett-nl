import React, { useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import "./scss/index.scss";
import { useSelector, shallowEqual } from "react-redux";
import { SideBar } from "./2-molecules/SideBar";
import { Content } from "./2-molecules/Content";
import { useDispatch } from "react-redux";
import * as controllers from "../controller";
import { useDivSize } from "../controller/hooks";
export function App() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const ref = useRef<HTMLDivElement>(null);
  useDivSize(ref);

  const pages = useSelector(
    (state: RootReducer) => state.pages,
    shallowEqual
  );

  const currentId = useSelector(
    (state: RootReducer) => state.id,
    shallowEqual
  );

  const viewport = useSelector(
    (state: RootReducer) => state.viewport,
    shallowEqual
  );

  const currentPage = pages.get(currentId);

  useEffect(() => {
    navigate(currentPage.url);
  }, [currentPage, navigate]);

  const clickHandler =
    (controller: (event: React.SyntheticEvent) => void) =>
    (event: React.SyntheticEvent) => {
      dispatch(controller(event));
    };

  return (
    <div className="App" ref={ref}>
      <SideBar
        viewport={viewport}
        controllers={controllers}
        clickHandler={clickHandler}
        currentId={currentId}
        pages={pages}
      />

      <Content
        viewport={viewport}
        controllers={controllers}
        clickHandler={clickHandler}
        currentPage={currentPage}
      ></Content>
    </div>
  );
}
