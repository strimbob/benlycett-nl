type Action = {
  type: string;
  payload: any;
};
type Dispatch = ({
  type,
  payload,
}: {
  type: string;
  payload: any;
}) => void;

type Image = {
  src: string;
  thumbnail: string;
  thumbnailWidth: number;
  thumbnailHeight: number;
};
type Page = {
  title: string;
  intro: string;
  paragraph: string;
  url: string;
  key: string;
  images?: Image[];
  videoLink?: string;
  link?: string;
};

type Viewport = {
  width: number;
  height: number;
  deviceType?: "phone" | "computer" | "unkown";
};
type Id = string;
type Pages = ImMap<Id, Page>;
type PageId = string | null;
type RootReducer = { id: PageId; pages: Pages; viewport: Viewport };
type ControlerType = Record<string, (event: React.SyntheticEvent) => void>;
type ClickHandler = ([k]: any) => void;
type StoreType = ReturnType<typeof rootReducer>;
