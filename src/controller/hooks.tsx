import { useEffect, useCallback } from "react";
import { useDispatch } from "react-redux";
import * as ActionTypes from "./actionTypes";
import { ViewportModel } from "../model/ViewportModel";
export const useResize = (callback: () => void) => {
  useEffect(() => {
    window.addEventListener("resize", callback);
    return () => {
      window.removeEventListener("resize", callback);
    };
  }, [callback]);
};

export const useDivSize = (
  div: React.RefObject<HTMLDivElement>,
  trigger?: any
) => {
  const dispatch = useDispatch();
  const resizeHandler = useCallback(() => {
    const { current } = div;
    if (current) {
      const viewport = ViewportModel.fromData({
        width: current.clientWidth,
        height: current.clientHeight,
      });
      dispatch({
        type: ActionTypes.VIEWPORT_UPDATE,
        payload: viewport,
      });
    }
  }, [div, dispatch]);

  useEffect(() => {
    resizeHandler();
  }, [trigger, resizeHandler]);

  useResize(resizeHandler);
};
