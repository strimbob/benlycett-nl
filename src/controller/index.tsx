import * as ActionTypes from "./actionTypes";

export const updateId =
  (event: React.SyntheticEvent) =>
  (dispatch: Dispatch, getState: () => RootReducer) => {
    const { id } = event.currentTarget;
    const page = getState().pages.get(id);
    if (!page) {
      dispatch({ type: ActionTypes.ID_UPDATE, payload: "unknown" });
      return;
    }
    dispatch({ type: ActionTypes.ID_UPDATE, payload: id });
  };

export const navPages =
  (event: React.SyntheticEvent) =>
  (dispatch: Dispatch, getState: () => RootReducer) => {
    const { id } = event.currentTarget;
    const pages = getState().pages;
    const currentId = getState().id;
    const currentPage = pages
      .entrySeq()
      .reduce((acc: number, _page: [Id, Page], index: number) => {
        const [_id] = _page;
        if (_id !== currentId) return acc;
        switch (id) {
          case "forward": {
            const newPage = index + 1;
            if (newPage >= pages.size - 1) {
              return 0;
            }
            return newPage;
          }
          case "back": {
            const newPage = index - 1;
            if (newPage < 0) {
              return pages.size - 2;
            }
            return newPage;
          }
          default:
            return acc;
        }
      }, 0);

    const [newPageid] = pages
      .entrySeq()
      .find((page: [Id, Page], index: number) => index === currentPage);

    updateId({
      ...event,
      currentTarget: { ...event.currentTarget, id: newPageid },
    })(dispatch, getState);
  };
