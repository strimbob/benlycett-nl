export const urlToID = ({ pages, url }: { pages: Pages; url: string }) => {
  return pages.entrySeq().reduce((acc: string, _page: [Id, Page]) => {
    const [id, page] = _page;
    if (page.url === url) return id;
    return acc;
  }, "unknown");
};
